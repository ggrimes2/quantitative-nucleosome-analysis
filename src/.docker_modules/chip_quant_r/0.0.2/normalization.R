#!/usr/bin/Rscript
library("tidyverse")
library("vroom")

# compute OR value
load_stat <- function(file) {
  vroom(
    file,
    col_names = c("value", "name", "type"),
    col_types = list("d", "f", "f")
  ) %>%
  filter(str_detect(type, "fasta.*")) %>%
  filter(str_detect(name, "^mapped$"))
}

or_data <- tibble(file = list.files()) %>%
  filter(str_detect(file, ".*\\.tsv")) %>%
  mutate(sample = ifelse(str_detect(file, "^IP_.*"), "ip", "w")) %>%
  mutate(mapping_stats = map(file, load_stat)) %>%
  unnest(c(mapping_stats))


extract_value <- function(data, sample_name, type_name) {
  data %>%
    filter(sample == sample_name & type == type_name) %>%
    pull(value)
}

get_or <- function(or_data, file) {
  or <- (
    (or_data %>% extract_value("w", "fasta_calib")) *
    (or_data %>% extract_value("ip", "fasta"))
  ) / (
    (or_data %>% extract_value("w", "fasta")) *
    (or_data %>% extract_value("ip", "fasta_calib"))
  )
  vroom_write(
    tibble(
      fasta_calib = c(
        or_data %>% extract_value("ip", "fasta_calib"),
        or_data %>% extract_value("w", "fasta_calib")
      ),
      fasta = c(
        or_data %>% extract_value("ip", "fasta"),
        or_data %>% extract_value("w", "fasta")
      ),
      type = c("ip", "w"),
      or = c(or, or)
    ),
    path = str_c(file, "_OR.tsv")
  )
  return(or)
}

# normalize bg files

norm_bg <- function(file, or) {
    vroom(
      file,
      col_names = c("chr", "start", "stop", "density"),
      col_types = list("f", "i", "i", "d")
    ) %>%
    mutate(density = density * or) %>%
    vroom_write(
      path = str_c("norm_", file),
      col_names = FALSE
    )
    return(0)
}

bg_files <- tibble(file = list.files()) %>%
  filter(str_detect(file, ".*\\.bg$")) %>%
  mutate(
    mapping_stats = map(file, function(x, or_data) {
      norm_bg(x, get_or(or_data, x))
    }, or_data = or_data)
  )

