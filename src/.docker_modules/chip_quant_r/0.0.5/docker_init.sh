#!/bin/sh
docker pull lbmc/chip_quant_r:0.0.5
docker build src/.docker_modules/chip_quant_r/0.0.5 -t 'lbmc/chip_quant_r:0.0.5'
docker push lbmc/chip_quant_r:0.0.5
