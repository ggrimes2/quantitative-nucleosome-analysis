#!/usr/bin/Rscript
library("tidyverse")
library("vroom")
library("rtracklayer")

read_cov <- function(x) {
  data <- rtracklayer::import(x)
  tibble(
    score = data$score,
    chromo = as.vector(seqnames(data))
  )
}

args = commandArgs(trailingOnly=TRUE)
ip_file = as.numeric(args[1])
wce_file = as.numeric(args[2])

norm_bg <- function(ip_file, wce_file) {
  wce_score <- vroom(
    wce_file,
    col_names = c("chr", "start", "stop", "score"),
    col_types = list("f", "i", "i", "d")
    ) %>%
    mutate(
      score = score + 1.0
    )
  vroom(
      ip_file,
      col_names = c("chr", "start", "stop", "score"),
      col_types = list("f", "i", "i", "d")
    ) %>%
    left_join(wce_score, by = c("chr", "start", "stop")) %>%
    mutate(score = score.x / score.y) %>%
    select(chr, start, stop, score) %>%
    vroom_write(
      path = str_c("ratio_", ip_file),
      col_names = FALSE
    )
  return(0)
}

norm_bg(ip_file, ip_wce)