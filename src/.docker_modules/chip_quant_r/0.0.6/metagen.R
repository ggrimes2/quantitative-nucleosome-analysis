#!/usr/local/bin/Rscript
library("tidyverse")
library("janitor")
library("data.table")

load_deeptools_mat <- function(file) {
  fread(file, skip = 2, nrows = 3, header = T, fill = T, drop = 1) %>% 
  names() %>%
  str_c("...", 1:length(.)) %>% 
  fread(
    file = file, skip = 4, header = F,
    col.names = .,
    fill = T)
}

format_mat <- function(mat) {
  mat %>%
    as_tibble(.name_repair = "unique", rownames = "gene") %>% 
    pivot_longer(cols = -gene) %>% 
    mutate(
      order = as.numeric(str_remove(name, ".*\\.\\.\\.")),
      name = as.factor(str_remove(name, "\\.\\.\\..*"))
    ) %>% 
    group_by(name, gene) %>% 
    arrange(order) %>% 
    mutate(pos = 1:n()) %>% 
    ungroup()
}

metagen_plot <- function(data) {
  data_plot <- data %>% 
    group_by(name, pos) %>% 
    summarise(
      av_value = mean(value, na.rm = T),
    ) %>% 
    ungroup() %>% 
    mutate(
      sample = gsub("IP_(.*)_\\d+_.*", "\\1", name),
      replicate = gsub("IP_.*_(\\d+)_L.*", "\\1", name)
    )
  data_plot %>% 
    ggplot(aes(x = pos * 10 - 2000, y = av_value, color = sample, group = name)) +
    geom_vline(xintercept = 0) +
    geom_vline(xintercept = 4000) +
    geom_label(aes(x = 0, y = 0), label = "TSS", color = "black") +
    geom_label(aes(x = 4000, y = 0), label = "TES", color = "black") +
    geom_line(size = 1.2) +
    geom_point(data = data_plot %>% filter(pos %% 50 == 0), aes(shape = replicate), size = 3) +
    labs(x = "position", y = "mean coverage") +
    theme_minimal()
}

args = commandArgs(trailingOnly=TRUE)
input_file = args[1]

print(input_file)

load_deeptools_mat(input_file) %>% 
  format_mat() %>% 
  metagen_plot()
ggsave(str_c(input_file, ".pdf"))

load_deeptools_mat(input_file) %>% 
  format_mat() %>% 
  metagen_plot() +
  scale_y_log10()
ggsave(str_c(input_file, "_log.pdf"))