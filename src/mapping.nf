/* ========================= modules import =================================*/
include {
  index_fasta;
  mapping_fastq;
} from './nf_modules/bowtie2/main'

include{
  mark_duplicate;
  mark_duplicate as mark_duplicate_calib;
} from './nf_modules/picard/main.nf'

params.output_bam_dir = ""
include {
  filter_bam;
  filter_bam as filter_bam_calib;
  filter_bam_mapped;
  sort_bam;
  sort_bam as sort_bam_monomapped;
  sort_bam as sort_bam_multimapped;
  index_bam;
} from './nf_modules/samtools/main' addParams(
  index_bam_out: params.output_bam_dir,
)

include {
  fasta_from_bed;
} from './nf_modules/bedtools/main'

/* ========================= processes =================================*/
process concatenate_genome {
  container = "lbmc/samtools:1.7"
  label 'big_mem_mono_cpus'
  tag "${file_id} ${calib_id}"
  publishDir "results/genome/fasta/", mode: 'copy', pattern: '*.fasta'
  publishDir "results/genome/bed/", mode: 'copy', pattern: '*.bed'

  input:
    tuple val(file_id), path(fasta)
    tuple val(calib_id), path(fasta_calib)

  output:
    tuple val(file_id), path("${fasta.simpleName}_${fasta_calib.simpleName}.fasta"), emit: fasta
    tuple val(file_id), path("${fasta.simpleName}.bed"), emit: bed_fasta
    tuple val(calib_id), path("${fasta_calib.simpleName}.bed"), emit: bed_fasta_calib

  script:
"""
# rename calib chr
zcat ${fasta_calib} | sed 's/>/>calib_/g' > fasta_calib.fasta
zcat ${fasta} > ${fasta.simpleName}.fasta

# build bed for fasta and fasta_calib
samtools faidx ${fasta.simpleName}.fasta
awk 'BEGIN {FS="\t"}; {print \$1 FS "0" FS \$2}' \
  ${fasta.simpleName}.fasta.fai > ${fasta.simpleName}.bed
samtools faidx fasta_calib.fasta
awk 'BEGIN {FS="\t"}; {print \$1 FS "0" FS \$2}' \
  fasta_calib.fasta.fai > ${fasta_calib.simpleName}.bed

# concat genomes
cat ${fasta.simpleName}.fasta fasta_calib.fasta > \
  ${fasta.simpleName}_${fasta_calib.simpleName}.fasta
"""
}

process filter_bam_multimapped {
  container = "lbmc/samtools:1.7"
  label 'big_mem_multi_cpus'
  tag "${file_id}"

  input:
    tuple val(file_id), path(bam)

  output:
    tuple val(file_id), path("*_monomapped.bam"), emit: monomapped_bam
    tuple val(file_id), path("*_multimapped.bam"), emit: multimapped_bam

  script:
"""
# sort by reads name and select non multimapped read accross the two genomes
samtools sort -n -@ ${task.cpus} -O SAM -o - ${bam} | \
awk '
  BEGIN{
    line_number = 0;
    fasta_mapping = 0;
    fasta_calib_mapping = 0;
  };
  \$0 ~ /^@/ {
    print \$0  >> "multimapped.sam";
    print \$0  >> "monomapped.sam";
  };
  \$0 ~ /^[^@]/ {
    if (\$1 != saved_reads[line_number]) {
      if (fasta_mapping > 0 && fasta_calib_mapping > 0 ) {
        for(line in saved_lines){
          print saved_lines[line] >> "multimapped.sam";
          delete saved_lines[line];
        }
      } else {
        for(line in saved_lines){
          print saved_lines[line] >> "monomapped.sam";
          delete saved_lines[line];
        }
      }
      line_number = 0;
      fasta_mapping = 0;
      fasta_calib_mapping = 0;
    }
    line_number += 1;
    saved_lines[line_number]=\$0;
    saved_reads[line_number]=\$1;
    if (\$3 ~ /calib_/){
      fasta_calib_mapping += 1;
    } else {
      fasta_mapping += 1;
    }
  };'
samtools view -@ ${task.cpus} -bh -O BAM -o ${bam.simpleName}_monomapped.bam monomapped.sam
samtools view -@ ${task.cpus} -bh -O BAM -o ${bam.simpleName}_multimapped.bam multimapped.sam
rm multimapped.sam monomapped.sam
"""
}

process rename_indexed_bam {
  tag "${file_id}"

  input:
    tuple val(file_id), path(bam), path(bam_idx)

  output:
    tuple val("${file_id.id}"), val("${file_id.mapping}"), path("*.bam"), path("*.bam.bai"), emit: bam_idx

  script:
"""
ln -s ${bam} ${bam.simpleName}_${file_id.mapping}.bam
ln -s ${bam_idx} ${bam_idx.simpleName}_${file_id.mapping}.bam.bai
"""
}

process group_indexed_bam {
  tag "$file_id"
  publishDir "results/mapping/bam/", mode: 'copy'

  input:
    tuple val(file_id), path(bam), path(bam_idx)

  output:
    tuple val(file_id), path("${file_id}/"), emit: bam_idx

  script:
"""
mkdir ${file_id}
mv *_all.bam ${file_id}/${file_id}_all.bam
mv *_all.bam.bai ${file_id}/${file_id}_all.bam.bai
mv *_both.bam ${file_id}/${file_id}_both.bam
mv *_both.bam.bai ${file_id}/${file_id}_both.bam.bai
mv *_fasta.bam ${file_id}/${file_id}_fasta.bam
mv *_fasta.bam.bai ${file_id}/${file_id}_fasta.bam.bai
mv *_fasta_calib.bam ${file_id}/${file_id}_fasta_calib.bam
mv *_fasta_calib.bam.bai ${file_id}/${file_id}_fasta_calib.bam.bai
"""
}

process rename_calib_bam {
  input:
  tuple val(file_id), path(bam)

  output:
  tuple val(file_id), path("*_calib.bam"), emit: bam

"""
mv ${bam} ${bam.baseName}_calib.bam
"""
}

/* ========================= workflow =================================*/

workflow mapping {
  take:
    fasta
    fasta_calib
    fastq
    input_csv

  main:
  // mapping
  concatenate_genome(fasta, fasta_calib)
  index_fasta(concatenate_genome.out.fasta)
  mapping_fastq(
    index_fasta.out.index.collect(),
    fastq
  )
  sort_bam(mapping_fastq.out.bam)

  // reads sorting
  filter_bam_mapped(sort_bam.out.bam)
  filter_bam_multimapped(
    filter_bam_mapped.out.bam
  )
  sort_bam_monomapped(filter_bam_multimapped.out.monomapped_bam)
  sort_bam_multimapped(filter_bam_multimapped.out.multimapped_bam)
  filter_bam(
    sort_bam_monomapped.out.bam,
    concatenate_genome.out.bed_fasta.collect()
  )
  filter_bam_calib(
    sort_bam_monomapped.out.bam,
    concatenate_genome.out.bed_fasta_calib.collect()
  )
  rename_calib_bam(filter_bam_calib.out.bam)
  if (params.perform_mark_duplicate != true) {
    filter_bam.out.bam
      .map{
        it -> [["id": it[0], "mapping": "fasta"], it[1]]
      }
      .set { bam_fasta_genome }
    rename_calib_bam.out.bam
      .map{
          it -> [["id": it[0], "mapping": "fasta_calib"], it[1]]
      }
      .set { bam_fasta_calib_genome }
  } else {
    mark_duplicate(filter_bam.out.bam)
    mark_duplicate_calib(rename_calib_bam.out.bam)
    mark_duplicate.out.bam
      .map{
        it -> [["id": it[0], "mapping": "fasta"], it[1]]
      }
      .set { bam_fasta_genome }
    mark_duplicate_calib.out.bam
      .map{
          it -> [["id": it[0], "mapping": "fasta_calib"], it[1]]
      }
      .set { bam_fasta_calib_genome }
  }

  sort_bam.out.bam
    .map{
      it -> [["id": it[0], "mapping": "all"], it[1]]
    }
    .mix(
      bam_fasta_genome
    )
    .mix(
      bam_fasta_calib_genome
    )
    .mix(
      sort_bam_multimapped.out.bam.map{
          it -> [["id": it[0], "mapping": "both"], it[1]]
      }
    )
    .set{ filtered_bam }
  
  index_bam(
    filtered_bam
  )

  if (params.perform_mark_duplicate) {
    mapping_fastq.out.report
      .mix(mark_duplicate.out.report)
      .mix(mark_duplicate_calib.out.report)
      .set{ mapping_report }
  } else {
    mapping_fastq.out.report
      .set{ mapping_report }
  }

  emit:
    bam = index_bam.out.bam_idx.map{ it -> [it[0], it[1]]}
    index = index_bam.out.bam_idx.map{ it -> [it[0], it[2]]}
    bam_idx = index_bam.out.bam_idx
    fasta = concatenate_genome.out.fasta
    bed_fasta = concatenate_genome.out.bed_fasta
    bed_fasta_calib = concatenate_genome.out.bed_fasta_calib
    report = mapping_report
}