/* ========================= modules import =================================*/
include {
  peak_calling as peak_calling_macs2
} from './nf_modules/macs2/main'

/* ========================= processes =================================*/
process merge_peaks {
  container = "lbmc/bedtools:2.25.0"
  label 'big_mem_mono_cpus'
  publishDir "results/peaks/", mode: 'copy'

  input:
    path narrowpeak

  output:
    path "*.narrowPeak", includeInputs: true, emit: peak

"""
ls -l | \
  grep "narrowPeak" | \
  sed 's/_sorted_dedup_mapped_monomapped_sorted_filtered_peaks.narrowPeak//g' | \
  awk '{system("mv "\$9"_sorted_dedup_mapped_monomapped_sorted_filtered_peaks.narrowPeak "\$9".narrowPeak")}'

cat *.narrowPeak | \
  bedtools sort -i - | \
  bedtools merge -i - > merged_peaks.narrowPeak
"""
}

process peak_calling_quantification {
  container = "lbmc/bedtools:2.25.0"
  label 'big_mem_mono_cpus'
  tag "$file_id"
  publishDir "results/peaks/", mode: 'copy'

  input:
    tuple val(file_id), path(bg_w_norm), path(bg_ip_norm)
    path narrowpeak

  output:
    tuple val(file_id), path("*.bed"), emit: bed

"""
bedtools intersect \
  -a ${bg_ip_norm} \
  -b merged_peaks.narrowPeak \
  -wa > ${file_id.IP}.bed
bedtools intersect \
  -a ${bg_w_norm} \
  -b merged_peaks.narrowPeak \
  -wa > ${file_id.WCE}.bed
"""
}

/* ========================= workflow =================================*/
workflow peak_calling {
  take:
    indexed_bam
    input_csv
    coverage_normalization

  main:
  indexed_bam
    .filter {it[0].mapping == "fasta"}
    .map{ it -> [it[0].id, it[1]]}
    .cross(
      input_csv
      .map{ it -> [it.id, it.group, it.ip, it.type, it.index] }
    )
    .map {
      it -> it[1] + [it[0][1]]
    }
    .map{ it -> [
      "index": it[4],
      "id": it[0],
      "group": it[1],
      "ip": it[2],
      "type": it[3],
      "file": it[5]
    ]}
    .map{ it -> [it.index, it.type, it]}
    .groupTuple(by: [0, 1])
    .map{ it -> it[2]}
    .map{ it ->
      if (it[1].ip == "WCE"){
        [
          [
            "index": it[0].index,
            "group": it[0].group,
            "type": it[0].type,
            "IP": it[0].id,
            "WCE": it[1].id
          ],
          it[0].file,
          it[1].file
        ]
      } else {
        [
          [
            "index": it[0].index,
            "group": it[0].group,
            "type": it[0].type,
            "IP": it[1].id,
            "WCE": it[0].id
          ],
          it[1].file,
          it[0].file
        ]
      }
    }
    .unique{ it[1] + it[2] }
    .set { merged_bam }

  if (params.perform_peak_calling) {
    peak_calling_macs2(
      merged_bam
    )
    merge_peaks(
      peak_calling_macs2.out.peak
      .map{ it -> it[1]}
      .collect()
    )
    peak_calling_quantification(
      coverage_normalization
      .map{ it ->
      [
        it[0],
        it[2],
        it[4]
      ]
      },
      merge_peaks.out.peak
      .collect()
    )
    peak_calling_macs2.out.report
    .set{ peak_calling_report }
  } else {
    channel.empty() 
    .set{ peak_calling_report }
  }
  emit:
    report = peak_calling_report
}