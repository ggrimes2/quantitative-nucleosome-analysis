#!/usr/bin/env nextflow




/*
 * Defines some parameters in order to specify the refence genomes
 * and read pairs by using the command line options
 */



//params.reads = "$baseDir/data/ggal/*_{1,2}.fq"
params.fastq="/home/narudo/ENS_stage/data/Tonelli/rep_*/*{1,6,4}_fastq*"
params.index = "/home/narudo/ENS_stage/quantitative-nucleosome-analysis/results/mapping/index/GCA_000002945.2_ASM294v2_genomic.*.ebwt"
params.outdir = 'results'


log.info """\
        M A P P I N G   P I P E L I N E
         =============================
         genome: ${params.index}
         fastq : ${params.fastq}
         outdir: ${params.outdir}
         """
         .stripIndent()

 Channel
   .fromPath( params.fastq )
   .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
   .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
   .set { fastq_files }
 Channel
   .fromPath( params.index )
   .ifEmpty { error "Cannot find any index files matching: ${params.index}" }
   .set { index_files }


/*
 *  Maps each fastq by using bowtie mapper tool
 */
 process mapping_fastq {
   tag "$file_id"
   publishDir "${params.outdir}/mapping/bams/", mode: 'copy'

   input:
   set file_id, file(reads) from fastq_files
   file index from index_files.collect()

   output:
   //set file_id, "*.bam" into bam_files ##danpos ne fonctionne pas avce bam
   set file_id, "*.bowtie" into bam_files
   file "*_report.txt" into mapping_report

   script:
 index_id = index[0]
 for (index_file in index) {
   if (index_file =~ /.*\.1\.ebwt/ && !(index_file =~ /.*\.rev\.1\.ebwt/)) {
       index_id = ( index_file =~ /(.*)\.1\.ebwt/)[0][1]
   }
 }
 """
 bowtie -p ${task.cpus} ${index_id} \
 -q ${reads} 2> \
 ${file_id}_bowtie_report_tmp.txt > ${file_id}.bowtie

 ##output BAM fonctionne pas avec DANPOS
 #bowtie --sam -p ${task.cpus} ${index_id} \
 #-q ${reads} 2> \
 #${file_id}_bowtie_report_tmp.txt | \
 #samtools view -Sbh - > ${file_id}.bam

 if grep -q "Error" ${file_id}_bowtie_report_tmp.txt; then
   exit 1
 fi
 tail -n 19 ${file_id}_bowtie_report_tmp.txt > ${file_id}_bowtie_report.txt
 """
 }
