#!/usr/bin/env nextflow


/*
 * Defines some parameters in order to specify the refence genomes
 * and read pairs by using the command line options
 */



 params.bam="/home/narudo/T*chargements/test_report/ERR23_sple.bam"



 log.info "bams files : ${params.bam}"

 Channel
   .fromPath( params.bam )
   .ifEmpty { error "Cannot find any bam files matching: ${params.bam}" }
   .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
   .set { bam_files }

   process get_nb_reads {
     label "samtools"
     input:
     set file_id, file(bam) from bam_files

      output:
      set file_id, stdout into nb_reads_mapp

      script:
      """
      samtools view -F 0x4 ${bam} | cut -f 1 | sort | uniq | wc -l
      """
   }

randNums.view()
   //nb_reads_mapp.view()



/*


b=grep "aligned exactly 1 time" report | cut -d "(" -f 1
a=$(grep "aligned >1 times" report | cut -d "(" -f 1)
echo $(( $a + $b))
*/
